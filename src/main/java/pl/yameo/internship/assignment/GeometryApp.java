package pl.yameo.internship.assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GeometryApp {
    private Scanner scanner;
    private List<Shape> shapes = new ArrayList<>();

    public GeometryApp(Scanner scanner) {
        this.scanner = scanner;
    }

    public void start() {
        boolean run = true;
        while (run) {
            run = run();
        }
    }

    private boolean run() {
        System.out.println("Choose action:");
        System.out.println("1) Create new shape");
        System.out.println("2) List existing shapes");
        System.out.println("3) Modify one of the shapes from the list");
        System.out.println("0) Exit");

        int option = readInteger();
        switch (option) {
            case 0:
                return false;
            case 1:
                Shape newShape = createNewShape();
                if (newShape != null) {
                    shapes.add(newShape);
                }
                break;
            case 2:
                listShapes();
                break;
            case 3:
                modifyShape();
                break;
        }

        return true;
    }

    private Shape createNewShape() {
        System.out.println("Choose shape to create:");
        System.out.println("1) Ellipse");
        System.out.println("2) Rectangle");
        System.out.println("3) Circle");
        System.out.println("4) Square");
        System.out.println("5) Triangle");
        System.out.println("6) Equilateral Triangle");
        System.out.println("0) Back");
        int option = readInteger();

        try {

            switch (option) {
                case 1:
                    return createNewEllipse();
                case 2:
                    return createNewRectangle();
                case 3:
                    return createNewCircle();
                case 4:
                    return createNewSquare();
                case 5:
                    return createNewTriangle();
                case 6:
                    return createNewEquilateralTriangle();
                default:
                    return null;
            }

        } catch (IllegalArgumentException e) {
            System.out.println("Exception occured");
            return null;
        }
    }


    private void listShapes() {
        System.out.println("====== LIST OF SHAPES ======");
        shapes.forEach(this::printShapeDetails);
    }

    private void modifyShape() {
        if (shapes.isEmpty()) {
            System.out.println("No available shapes");
            return;
        }
        listShapes();
        System.out.println("Please choose the index of the shape you want to modify (1-" + shapes.size() + "): ");
        int index;
        while (true) {
            index = readInteger();
            if (index >= 1 && index <= shapes.size()) {
                break;
            }
            System.out.println("Wrong index");
        }
        Shape activeShape = shapes.get(index - 1);

        printShapeDetails(activeShape);

        try {
            Shape shape = activeShape.accept(new ShapeVisitor<Shape>() {
                @Override
                public Shape accept(Circle circle) {
                    System.out.println("Please provide the radius for the circle:");
                    return new Circle(readDouble());
                }

                @Override
                public Shape accept(Ellipse ellipse) {
                    System.out.println("Please provide two semi-axis lengths (major, minor):");
                    return new Ellipse(readDouble(), readDouble());
                }

                @Override
                public Shape accept(Rectangle rectangle) {
                    System.out.println("Please provide two edge lengths (height, width):");
                    return new Rectangle(readDouble(), readDouble());
                }

                @Override
                public Shape accept(Square square) {
                    System.out.println("Please provide the edge length:");
                    return new Square(readDouble());
                }

                @Override
                public Shape accept(Triangle triangle) {
                    System.out.println("Please provide three edge lengths:");
                    return new Triangle(readDouble(), readDouble(), readDouble());
                }

                @Override
                public Shape accept(EquilateralTriangle equilateralTriangle) {
                    System.out.println("Please provide edge of Equilateral Triangle");
                    return new EquilateralTriangle(readDouble());
                }
            });

            shapes.set(index - 1, shape);

            System.out.println("Old shape: ");
            printShapeDetails(activeShape);
            System.out.println("New shape: ");
            printShapeDetails(shape);

        } catch (IllegalArgumentException e) {
            System.out.println("Exception occured");
        }

    }

    private Ellipse createNewEllipse() {
        System.out.println("Please provide two semi-axis lengths (major, minor):");
        return new Ellipse(readDouble(), readDouble());
    }

    private Rectangle createNewRectangle() {
        System.out.println("Please provide two edge lengths (height, width):");
        return new Rectangle(readDouble(), readDouble());
    }

    private Circle createNewCircle() {
        System.out.println("Please provide the radius for the circle:");
        return new Circle(readDouble());
    }

    private Square createNewSquare() {
        System.out.println("Please provide the edge length:");
        return new Square(readDouble());
    }

    private Triangle createNewTriangle() {
        System.out.println("Please provide three edge lengths:");
        return new Triangle(readDouble(), readDouble(), readDouble());
    }

    private Shape createNewEquilateralTriangle() {
        System.out.println("Please provide edge length:");
        return new EquilateralTriangle(readDouble());
    }

    private int readInteger() {
        while (true) {
            if (scanner.hasNextInt()) {
                return scanner.nextInt();
            } else {
                scanner.next();
            }
        }
    }

    private double readDouble() {
        while (true) {
            if (scanner.hasNextDouble()) {
                return scanner.nextDouble();
            } else {
                scanner.next();
            }
        }
    }

    private void printShapeDetails(Shape shape) {
        System.out.print(shape.getName() + " with dimensions: ");
        System.out.print(shape.listDimensions() + "; ");
        System.out.print("Area: " + shape.calculateArea() + "; ");
        System.out.println("Perimeter: " + shape.calculatePerimeter());
        System.out.println("============================");
    }
}
