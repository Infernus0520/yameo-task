package pl.yameo.internship.assignment;

/**
 * Created by Lukasz on 05.01.2018.
 */
public interface ShapeVisitor<T> {
   <T> T accept(Circle circle);
   <T> T accept(Ellipse ellipse);
   <T> T accept(Rectangle rectangle);
   <T> T accept(Square square);
   <T> T accept(Triangle triangle);
   <T> T accept(EquilateralTriangle equilateralTriangle);
}
