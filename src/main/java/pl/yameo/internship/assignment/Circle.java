package pl.yameo.internship.assignment;

public class Circle extends Ellipse {
	public Circle(double radius) {
		super(radius, radius);
	}

	@Override
	public String getName() {
		return "Circle";
	}

	@Override
	public <T> T accept(ShapeVisitor<T> vistor) {
		return vistor.accept(this);
	}

}
