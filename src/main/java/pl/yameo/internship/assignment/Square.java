package pl.yameo.internship.assignment;

public class Square extends Rectangle {
	public Square(double dimension) {
		super(dimension, dimension);
	}

	@Override
	public String getName() {
		return "Square";
	}

	@Override
	public <T> T accept(ShapeVisitor<T> vistor) {
		return vistor.accept(this);
	}

}
