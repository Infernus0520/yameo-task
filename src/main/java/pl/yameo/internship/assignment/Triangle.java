package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

public class Triangle implements Shape {
	private final double edgeA;
	private final double edgeB;
	private final double edgeC;


	public Triangle(double edgeA, double edgeB, double edgeC) {
		if(edgeA <= 0) {
			throw new IllegalArgumentException("edgeA must be greater than 0");
		}
		if(edgeB <= 0) {
			throw new IllegalArgumentException("edgeB must be greater than 0");
		}
		if(edgeC <= 0) {
			throw new IllegalArgumentException("edgeC must be greater than 0");
		}

		if (edgeA + edgeB <= edgeC || edgeA + edgeC <= edgeB || edgeB + edgeC <= edgeA) {
			throw new IllegalArgumentException("At least one edge must be lesser than two others");
		}
		this.edgeA = edgeA;
		this.edgeB = edgeB;
		this.edgeC = edgeC;
	}

	@Override
	public String getName() {
		return "Triangle";
	}

	@Override
	public final List<Double> listDimensions() {
		return Arrays.asList(edgeA, edgeB, edgeC);
	}

	@Override
	public final double calculateArea() {
		Double halfPerimeter = calculatePerimeter() / 2;
		return Math.sqrt(halfPerimeter * (halfPerimeter - edgeA) * (halfPerimeter - edgeB) * (halfPerimeter - edgeC));
	}

	@Override
	public <T> T accept(ShapeVisitor<T> vistor) {
		return vistor.accept(this);
	}

	@Override
	public final double calculatePerimeter() {
		return edgeA + edgeB + edgeC;
	}

}
