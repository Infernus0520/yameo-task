package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

public class Ellipse implements Shape {
	private final double semiMajorAxis;
	private final double semiMinorAxis;

	public Ellipse(double semiMajorAxis, double semiMinorAxis) {
		if(semiMajorAxis <= 0) {
			throw new IllegalArgumentException("semiMajorAxis must be greater than 0");
		}
		if(semiMinorAxis <= 0) {
			throw new IllegalArgumentException("semiMinorAxis must be greater than 0");
		}
		this.semiMajorAxis = semiMajorAxis;
		this.semiMinorAxis = semiMinorAxis;
	}

	@Override
	public String getName() {
		return "Ellipse";
	}

	@Override
	public final List<Double> listDimensions() {
		return Arrays.asList(semiMajorAxis, semiMinorAxis);
	}

	@Override
	public final double calculateArea() {
		return Math.PI * (3 * (semiMajorAxis + semiMinorAxis) / 2 - Math.sqrt(semiMajorAxis * semiMinorAxis));
	}

	@Override
	public <T> T accept(ShapeVisitor<T> visitor) {
		return visitor.accept(this);
	}

	@Override
	public final double calculatePerimeter() {
		return Math.PI * semiMajorAxis * semiMinorAxis;
	}

}
