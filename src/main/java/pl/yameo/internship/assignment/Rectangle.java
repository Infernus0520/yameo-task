package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

public class Rectangle implements Shape {
	private final double height;
	private final double width;

	public Rectangle(double height, double width) {
		if(height <= 0) {
			throw new IllegalArgumentException("Height must be greater than 0");
		}
		if(width <= 0) {
			throw new IllegalArgumentException("Width must be greater than 0");
		}
		this.height = height;
		this.width = width;
	}

	@Override
	public String getName() {
		return "Rectangle";
	}

	@Override
	public final List<Double> listDimensions() {
		return Arrays.asList(height, width);
	}

	@Override
	public final double calculateArea() {
		return height * width;
	}

	@Override
	public final double calculatePerimeter() {
		return 2 * (height + width);
	}

	@Override
	public <T> T accept(ShapeVisitor<T> vistor) {
		return vistor.accept(this);
	}

}
