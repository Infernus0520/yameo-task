package pl.yameo.internship.assignment;

/**
 * Created by Lukasz on 05.01.2018.
 */
public class EquilateralTriangle extends Triangle {
    public EquilateralTriangle(double edgeA) {
        super(edgeA, edgeA, edgeA);
    }

    @Override
    public String getName() {
        return "Equilateral Triangle";
    }

    @Override
    public <T> T accept(ShapeVisitor<T> vistor) {
        return vistor.accept(this);
    }
}
