package pl.yameo.internship.assignment;

import org.junit.Assert;
import org.junit.Test;

public class TriangleTest {
	private static final double initialEdgeA = 3.0;
	private static final double initialEdgeB = 4.0;
	private static final double initialEdgeC = 5.0;

	@Test
	public void when_triangle_is_created_then_proper_dimensions_are_returned() {
		Triangle triangle = new Triangle(initialEdgeA, initialEdgeB, initialEdgeC);

		Assert.assertEquals(initialEdgeA, triangle.listDimensions().get(0), 0.0001);
		Assert.assertEquals(initialEdgeB, triangle.listDimensions().get(1), 0.0001);
		Assert.assertEquals(initialEdgeC, triangle.listDimensions().get(2), 0.0001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void when_triangle_with_edgeA_greater_than_two_others_created_then_exception_is_thrown() {
		new Triangle(initialEdgeC, 1.0, initialEdgeA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void when_triangle_with_edgeB_greater_than_two_others_created_then_exception_is_thrown() {
		new Triangle(initialEdgeA, initialEdgeC, 1.0);
	}


	@Test(expected = IllegalArgumentException.class)
	public void when_triangle_with_edgeC_greater_than_two_others_created_then_exception_is_thrown() {
		new Triangle(initialEdgeA, 1.0, initialEdgeC);
	}

}
