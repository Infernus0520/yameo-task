package pl.yameo.internship.assignment;

import org.junit.Assert;
import org.junit.Test;

public class SquareTest {
	private static final double INITIAL_DIMENSION = 4.0;
	private static final String SHAPE_NAME = "Square";


	@Test
	public void when_square_created_then_it_has_proper_name() {
		Square square = new Square(INITIAL_DIMENSION);
		Assert.assertEquals(square.getName(), SHAPE_NAME);
	}

	// Objects are immutable , so tests does not have any sense

/*	@Test
	public void when_height_set_then_dimension_is_changed() {
		Square square = new Square(INITIAL_DIMENSION);
		square.setHeight(NEW_EDGE_LENGTH);
		Assert.assertEquals(square.listDimensions().get(0), NEW_EDGE_LENGTH, DELTA);
		Assert.assertEquals(square.listDimensions().get(1), NEW_EDGE_LENGTH, DELTA);
	}

	@Test
	public void when_width_set_then_dimension_is_changed() {
		Square square = new Square(INITIAL_DIMENSION);
		square.setWidth(NEW_EDGE_LENGTH);
		Assert.assertEquals(square.listDimensions().get(0), NEW_EDGE_LENGTH, DELTA);
		Assert.assertEquals(square.listDimensions().get(1), NEW_EDGE_LENGTH, DELTA);
	}

	@Test
	public void when_width_set_then_new_dimension_is_returned() {
		Square square = new Square(INITIAL_DIMENSION);
		square.setDimension(NEW_EDGE_LENGTH);
		Assert.assertEquals(square.listDimensions().get(0), NEW_EDGE_LENGTH, DELTA);
		Assert.assertEquals(square.listDimensions().get(1), NEW_EDGE_LENGTH, DELTA);
	}*/

	/*@Test
	public void when_rectangle_width_is_halved_then_its_area_is_halved() {
		Rectangle square = new Square(INITIAL_DIMENSION);
		Assert.assertEquals(square.calculateArea(), INITIAL_AREA, DELTA);
		square.setWidth(INITIAL_DIMENSION / 2);
		Assert.assertEquals(square.calculateArea(), INITIAL_AREA / 2, DELTA);
	}*/
}
